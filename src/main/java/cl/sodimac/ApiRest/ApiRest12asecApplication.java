package cl.sodimac.ApiRest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiRest12asecApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiRest12asecApplication.class, args);
	}

}

package cl.sodimac.ApiRest.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiRestController {
	
	@GetMapping("**/")
	public ResponseEntity<Object> getName(){
		
		Object obj = new String("12asec...");
		
		ResponseEntity<Object> salida = new ResponseEntity<Object>(obj, HttpStatus.OK);
		
		return salida;
		
	}

}

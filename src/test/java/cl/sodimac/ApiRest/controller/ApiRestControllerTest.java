package cl.sodimac.ApiRest.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApiRestControllerTest {

	private ApiRestController controlador;
	
	@BeforeEach
	private void init() {
		
		controlador = new ApiRestController();
		
	}
	
	@Test
	void test1() {
	
		assertEquals(controlador.getName().getBody().toString(), "12asec...");
		
	}
	
	@Test
	void test2() {
	
		assertEquals(controlador.getName().getStatusCode(), HttpStatus.OK);
		
	}

}
